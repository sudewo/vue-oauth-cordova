import oauth from './src/oauth'
export default {
  install (Vue, options) {
    // console.log('OAUTH PLUGIN LOADED!', options)
    Vue.prototype.$oauth = oauth
    Vue.prototype.$oauth.init(options, new Vue())
  }
}
