# Vue Oauth Cordova
   - login with google, facebook, instagram using oauth provider
   - support cordova using inappbrowser, tested in ios android simulator
   - now just for testing and learning, not ready in production

## Install

```bash
    yarn add vue-auth-cordova
```
for cordova, first install inappbrowser plugin  `cordova plugin add cordova-inappbrowser`

## Usage
1. register vue plugin    
    ```js
        import oauth from 'vue-auth-cordova'
        //required parameter every platform `client_id, client_secret, redirect_uri`
        Vue.use(oauth, {
            clearCacheInappBrowserBeforeLogin: false, //only for cordova
            facebook: {
                client_id: process.env.FACEBOOK_CLIENT_ID,
                client_secret: process.env.FACEBOOK_CLIENT_SECRET,
                redirect_uri: process.env.FACEBOOK_REDIRECT_URI
            },
            google: {
                client_id: process.env.GOOGLE_CLIENT_ID,
                client_secret: process.env.GOOGLE_CLIENT_SECRET,
                redirect_uri: process.env.GOOGLE_REDIRECT_URI,
                scope: ['https://www.googleapis.com/auth/youtube.readonly']// add new scope
            },
            instagram: {
                client_id: process.env.IG_CLIENT_ID,
                client_secret: process.env.IG_CLIENT_SECRET,
                redirect_uri: process.env.IG_REDIRECT_URI
            }
        })
    ``` 
2. using plugin
  - using oauth method
    ```js
     // @click.prevent="$oauth.loginWith('facebook')"
     // @click.prevent="$oauth.loginWith('google')"
     // @click.prevent="$oauth.loginWith('instagram')"
    ```
  - login with facebook
    ```js
        <q-btn label="Login with Facebook"
                 class="bg-facebook text-white full-width q-py-sm"
                 unelevated
                 no-caps
                 icon="img:statics/svg/nucleo/logo-facebook-glyph-32-tuatara.svg"
                 @click.prevent="$oauth.loginWith('facebook')" />
    ```
  - listen response
    ```js
            mounted() {
                this.$oauth.bus.$on('OAUTH_RESPONSE', this.oauthResponse)
            },
            beforeDestroy() {
                this.$oauth.bus.$off('OAUTH_RESPONSE')
            },
            methods: {
                getUserProfile(val) {
                    switch (val.type) {
                        case 'google':
                        return { id: val.data.id, name: val.data.name, email: val.data.email, picture: val.data.picture, type: val.type }
                        case 'facebook':
                        return { id: val.data.id, name: val.data.name, email: val.data.email, picture: val.data.picture.data.url, type: val.type }
                        case 'instagram':
                        return { id: val.data.id, name: val.data.full_name, email: val.data.username, picture: val.data.profile_picture, type: val.type }
                    }
                },
                oauthResponse(val) {
                    console.log('OAUTH RESPONSE', val)
                    if (val.status === 'success') {
                        let data = this.getUserProfile(val)
                        this.$q.notify(`Success Login as ${data.name}`)
                        this.$router.replace({ path: '/verify-user', query: data })
                    } else {
                        this.$q.notify('Login Failed')
                    }
                }
            }
    ```
   - format response
      - format error
         ```js
                {
                    "status": "error",
                    "type": "facebook",
                    "data": "",
                    "errorMessage": {
                        "error": {
                        "message": "Invalid OAuth access token.",
                        "type": "OAuthException",
                        "code": 190,
                        "fbtrace_id": "AmAUXDKkiDSAd73MS5fwpT3"
                        }
                    }
                }
        ``` 
      - format success
        ```js
            {
                "status": "success",
                "type": "facebook",
                "data": {
                    "access_token":"xxxx"
                    "state":{
                        "type":"fb"
                        }
                    "id": "7490......",
                    "name": "John Do",
                    "email": "jone.do@gmail.com"
                },
                "errorMessage": ""
            }
        ```    
   
      - this plugin only return basic profile from oauth provider, if you want to get custom scope like youtube channel, the success response return `acces_token`, you can use it to fetch other scope manually, for now custom scope only for google.


        1. first add youtube scope in oauth parameter
      
          ```js
              google: {
                  client_id: process.env.GOOGLE_CLIENT_ID,
                  client_secret: process.env.GOOGLE_CLIENT_SECRET,
                  redirect_uri: process.env.GOOGLE_REDIRECT_URI,
                  scope: ['https://www.googleapis.com/auth/youtube.readonly']
              },
          ```

        2. get youtube channel   
            ```js
                this.$oauth.bus.$on('OAUTH_RESPONSE', async (response) => {
                       try {
                            let getYoutubeDetail = await fetch(
                            `https://www.googleapis.com/youtube/v3/channels?mine=true&part=id,snippet&access_token=${accessToken}&key=YOUTUBE_API_KEY`,
                            {
                                referrer: 'http://localhost:8080/login',
                                referrerPolicy: 'unsafe-url'
                            }
                            )
                            let response = await getYoutubeDetail.json()
                            return response
                        } catch (e) {
                            return e
                        }
                })
            ```

            note: enable restrict key to prevent quota limit youtube v3
            
3. how to switch account within cordova inappbrowser

    - facebook : the flow is logout facebook then login again with different account, add options value for facebook relogin
        ```js
        this.$oauth.loginWith('fb', { relogin:true, token:'LOGIN ACCCES TOKEN' })
        ```  
    - instagram : same with facebook add relogin parameter
        ```js
        this.$oauth.loginWith('instagram', { relogin:true, token:'LOGIN ACCCES TOKEN' })
        ```
    - google : google oauth is automatically display prompt to switch another account   
    
    - you can add `state` parameter to oauth options
        ```js
        let options = { ...others, state: { redirect_to: redirectTo } }
        this.$oauth.loginWith(type, options)
        ```
        get state parameter from oauth response
        ```js
        "data": {
            "access_token":"xxxx"
            "state":{
                "type":"facebook",
                "redirect_to": "http://localhost/connect"
                }
        ```